## Description

**Issues** :

## Checklist before merge :

-   [ ] No remaining `console.log`
-   [ ] Version changed for any feature or bugfix
-   [ ] Create an entry in [changelog](/docs/changelog.md)
-   [ ] english and french translations if required
-   [ ] all eslintConfig rules are on ("2") (except `no-useless-escape` / `vue/no-mutating-props`)
-   [ ] `npm run prettier:write` done

## Tests

-   [ ] Aktivisda can be used: start aktivisda and play with it
-   [ ] Backtivisda can be used: start backtivisda and play with it
-   [ ] If it’s a bug, a new test added
-   [ ] If it’s a new feature, a new test added
