# Generator Aktivisda

A yeoman package to generate a repository

1. Installer `yeoman`

```
npm install -g yo
```

2. Installer le générateur :

```
git clone <ce repo>
cd generator-aktivisda
npm install
npm link
```

3. Go to the local folder where you want to create your instance :

```
yo aktivisda
```
