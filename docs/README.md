# Aktivisda.earth

_Aktivisda_ is a web application that makes it easy to create and distribute visuals based on an organisation visual identity.

Your organization members can create posters in a few minutes while designers can find the complete visual identity (colors, fonts, logos, etc.) and import them in their favorite sofware.

Aktivisda is a [Free Sofware](https://www.gnu.org/philosophy/free-sw.html). It means that every one "have the freedom to run, copy, distribute, study change and improve the sofware": everything you need (including this documentation) is available on [Framagit](https://framagit.org/aktivisda/aktivisda).
