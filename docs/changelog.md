# Changelog

## 1.0.33 (2023-10-07)

Feat (backtivisda): Existing templates can now be updated (labels, tags or image themselves) (see issue [#173](https://framagit.org/aktivisda/aktivisda/-/issues/173))

Doc: New [specifications page](/fr/admin/specifications) with all the features/roadmap of backtivisda

## 1.0.32 (2023-10-12)

Feature:

-   it’s now possible to upload local png image using the image picker component (issue [#144](https://framagit.org/aktivisda/aktivisda/-/issues/144))
-   all png images (loaded or exported) are compressed using oxipng (issue [#52](https://framagit.org/aktivisda/aktivisda/-/issues/52))

## 1.0.31 (2023-09-07)

Bug Fixes:

-   "Download Button" in symbol picker was broken ([#171](https://framagit.org/aktivisda/aktivisda/-/issues/171))
-   Application was broken (blank bage) if browser language and 'en' are not in available langs (this is the case for 'french only' instances). ([#146](https://framagit.org/aktivisda/aktivisda/-/issues/146))
-   Fix a `console.error` while loading editor without any template: use default opengraph image.

Doc:

-   Create a new [tests](/dev/tests) page in the documentation with very basic information about testing (and cypress)
