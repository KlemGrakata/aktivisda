# Tests

Aktivisda does not have a lot of tests yet but some E2E tests are defined with [Cypress](https://www.cypress.io/).

Tests are currently not run in CI.

## Getting Started

-   Start cypress

```terminal
npm run cypress:open
```

-   Start Aktivisda on port `8080`

```terminal
npm run serve:aktivisda
```

The tests are defined in `cypress` folder.
