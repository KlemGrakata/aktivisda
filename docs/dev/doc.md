# Documentation

This documentation is based on [docsify](https://docsify.js.org/) which is simple and lightweight.

## Getting Started

-   Start the docs locally

```terminal
npm run serve:docs
```

📚 [Documentation](https://docsify.js.org/)

## Edit the documentation

You can create or edit markdown files directly in `/docs` folder.
