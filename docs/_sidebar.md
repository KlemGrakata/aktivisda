-   **Getting started**

    -   [Aktivisda](/)
    -   [Showcase](/showcase)
    -   [Quick start](/gettingstarted/quickstart)
    -   [Deploy](/gettingstarted/deploy)
    -   [Backtivisda](/gettingstarted/backtivisda)

-   **Customization**

    -   [Backtivisda](/fr/admin/backtivisda)
    -   [Specifications](/fr/admin/specifications)
    -   [Fonts](/admin/fonts)
    -   [Colors and palettes](/admin/colors)
    -   [Symbols/images](/admin/symbols)
    -   [Backgrounds](/admin/backgrounds)
    -   [Categories](/admin/tags)

-   **Development**

    -   [Documentation](/dev/doc)
    -   [Tests](/dev/tests)

-   [Changelog](/changelog)
