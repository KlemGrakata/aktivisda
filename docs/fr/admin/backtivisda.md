# Backtivisda

Backtivisda est le [Système de Gestion de Contenu](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_gestion_de_contenu) (CMS) de Aktivisda. Il permet d'administrer quotidiennement votre instance de Aktivisda (modifier les images, créer de nouveaux modèles, etc.)

Backtivisda n’est qu’une interface capable de modifier les données présentes dans un repository Gitlab. Modifier directement les données conduira au même effet. Néanmoins, il faut avoir en tête que manipuler des données n’est pas simple.

En effet, une image par exemple est référencée à quatre endroits :

-   dans le fichier `symbols.json` où sont indiquées les méta-données de l’image : ses dimensions, ses catégories, son titre, etc.
-   dans le dossier `symbols` : directement l’image
-   une miniature en formats `.png` ou `.jpg` dans le dossier `symbols/previews`
-   une miniature (très réduite) en format `.webp` dans le dossier `symbols/thumbnails`

Il faut :

-   générer les images au bon format et aux bonnes résolutions ;
-   s’assurer que les fichiers soient aux bons endroits et avec le bon nom ;

👉 Backtivisda permet de faire tout ça.

## Fonctionnement

En pratique, Backtivisda est une version modifiée d'Aktivisda où

1. Les données sont **toujours synchronisées** avec votre repositoy : on utilise l’API de Gitlab pour récupérer ces données
2. Il y a des fonctionnalités supplémentaires

Backtivisda inter-agit donc avec l’instance Gitlab qui hébérge vos donées **et** avec un serveur où sont effectuées les opérations distantes (compression d'image, vectorisation, etc.)

### Pré-requis

Pour fonctionner vous avez besoin de deux token pour vous authentifier :

-   auprès de Gitlab : pour cela créer un _token d’accès_ au projet.
-   auprès du serveur de traitement d’images : soit vous disposez de votre propre serveur, soit vous pouvez demander à dev@aktivisda.earth un token

### Principe de fonctionnement

!> **Bon à savoir** pour le moment, les modifications ne sont pas sauvegardées sur votre ordinateur. Cela signifie en particulier que toutes vos modifications disparaissent à chaque fois que vous rafraîchissez votre navigateur (le fameux _F5_).

Voici ce qui se passe lorsqu’on souhaite faire une modification avec Backtivisda :

1. Se rendre sur `/backtivisda` ;
2. Le site est alors téléchargé par votre navigateur depuis un serveur où est disposé une version compilée de backtivisda.
3. Vous renseignez vos deux tokens (uniquement la première fois, ensuite ces tokens sont enregistrés dans voter navigateur) ;
4. Backtivisda va alors télécharger les données depuis Gitlab. Les données sont téléchargées au fur et à mesure, uniquement lorsque nécessaire (par ex. les images de fond sont téléchargées lorsqu'on se rend sur cette page)
5. Vous faites les manipulations que vous souhaitez : ajout ou modificatino d’images, de modèles, etc.
6. Une fois satisfait·e, vous vous rendez sur la page _Synchroniser_, vérifiez vos modifications, écrivez un message et cliquez sur _Synchroniser_
7. Un **commit** est créé et transmis à Gitlab. Il s’agit d’un paquet de toutes vos modifications. Un point de sauvegarde est aussitôt créé, ce qui permet éventuellement d’annuler une modification ;
8. Ce commit créée une _Pipeline_ de compilation (regarder la section _Pipelines_) sur Gitlab
9. Cette pipeline attend qu’un _worker_ soit disponible. Aujourd’hui, pour des contraintes techniques, ce _worker_ n'est pas tout le temps disponible. Au bout de 4h, si aucun worker n'est disponible, l'intégration continue échoue ;
10. Une fois le worker disponible, Aktivisda est compilé en prenant en compte les nouvelles données (quelques minutes) ;
11. Le nouveau site est transféré depuis le worker vers le serveur en charge du service (via FTP ou SSH) ;
12. Ça y est, Aktivisda est disponible ! Vous pouvez vous en rendre compte en regardant la date _Last Update_ dans la barre de navigation de Aktivisda
