# Fonctionnalités de backtivisda

> Backtivisda permet de faire beaucoup de choses ,mais pas tout. Cette page dresse un état des lieux de ce qu’il est possible / pas encore possible de faire.

## Modèles

**Possible** :

-   ajouter un nouveau modèle à partir de l'éditeur _de modèles_. Le modèle peut-être lui même chargé depuis un `.json` ;
-   modification d'un modèle déjà présent en base de données (ticket [#173](https://framagit.org/aktivisda/aktivisda/-/issues/173))

**À venir / non-supporté / envisagé** :

-   Suppression d'un modèle
-   Modification partielle d'un modèle : ne pas regénérer les différentes images si on a juste changé le label

[En savoir plus](/admin/templates)

## Images/symboles

**Possible** :

-   Ajouter une image `.svg` telle quelle ;
-   Ajouter une image `.png` ou `.jpg` ;
-   Ajout d'une image `.png` et vectorisation de celle-ci

**À venir / non supporté / envisagé** :

-   Vérifier que l'image svg est correcte. Voire la compresser
-   Remplacer une image déjà existante
-   Changer les tags ou labels d'une image déjà existante

[En savoir plus](/admin/symbols)

## Image de fonds

**Possible** :

-   Ajouter une image `.svg` telle quelle ;
-   Ajouter une image `.png` ou `.jpg` ;
-   Ajout d'une image `.png` et vectorisation de celle-ci

**À venir / non supporté / envisagé** :

-   Vérifier que l'image svg est correcte. Voire la compresser
-   Remplacer une image déjà existante : notamment pour la vectoriser.
-   Changer les tags ou labels d'une image déjà existante
-   Ajout des images _locales_ (_ie._ intégrées aux templates) dans la galerie (voir ticket [#176](https://framagit.org/aktivisda/aktivisda/-/issues/176))

[En savoir plus](/admin/symbols)

## Polices de caractères

**Possible** : rien

**À venir** : compliqué à envisager, lié à la gestion particulière des polices qui, dans l'architecture actuelle du code ont besoin d'être disponibles en amont.

[En savoir plus](/admin/fonts)

## Couleurs et palettes

**Possible** : rien

**À venir** :

-   Modifier la liste des couleurs : ajouter, modifier, supprimer ajouter des tags
-   Créer de nouvelles palettes

## Tags

**Possible** :

-   Ajout de tags _via_ les formulaires d'ajout de symboles et d'images de fonds

**À venir** :

-   Page dédiée pour l’ajout de tags

## Sauvegarde

**Possible** :

-   Voir la liste de tous les éléments modifiés avec un petit badge (modified, new, deleted) pour chacun d’entre eux ;
-   Annuler une modification
-   Ajouter un message personnalisé de commit

**À venir** :

-   Pré-remplir le message de commit en fonction des modifications effectutées
-   Conserver dans le navigateur les modifications effectuées
-   Permettre d’exporter toutes les modifications dans un fichier de texte
-   Mettre en cache les requêtes gitlab effectuées (ticket [#175](https://framagit.org/aktivisda/aktivisda/-/issues/175))
