# Fonts

1. Download the font file. If you want to make it easy for the users to download and use the font on their own computer, you can provide your fonts in many formats. You can look to online fonts converter or to use [this one](https://framagit.org/Marc-AntoineA/files-manipulator/-/tree/main/fonts-manipulator) (Framagit link)

2. Add your entry to the json list (here for v1.0.11). File `local/data/fonts.json`

```json
{
"version": "1.0.11",
"fonts": [
    {
      "css": {
        "font-style": "normal",
        "font-weight": "normal",
        "font-display": null,
        "unicode-range": null
      },
      "fontName": "< font name>", // used in css
      "directory": "./<your font name>",
      "formats": {
        "otf": "<font name>.otf", // complete path static/fonts/<your font name>/<font name>.oth
        "woff": "<font name>.woff",
        "ttf": "<font name>.ttf"
      },
      "name": "<label font name>", // to display
      "loaded": false,
      "italic": false,
      "bold": false,
      "alwaysLoaded": true, // if your want is heavy and speacific (eg. chinese font), set alwaysLoaded: false
      "default": true
    },
    ...
]
}
```
