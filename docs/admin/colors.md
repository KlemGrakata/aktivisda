# Colors

Colors are defined in file `local/data/colors.json`. One entry for each color. Please provide only 6 characters html colors (letters in lowercase).
