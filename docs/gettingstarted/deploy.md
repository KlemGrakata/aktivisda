# Deploy aktivisda

Aktivisda (and backtivisda) is a static website without any database or any backend dependency.

By default, aktivisda is pre-generated and will be served at the root url: `yourinstance.com` and backtivisda at root `/backtivida` (eg: `yourinstance.com/backtivisda`).

It is recommanded to create one folder on your website with associated FTP user, and to serve this folder using nginx or apache2.

Once built (using `npm run build:aktivisda` / `npm run build:backtivisda`) you just have to move the content of `dist/aktivisda` and `dist/backtivisda` on your server.

## Continuous Integration

You can find deploy scripts in Aktivisda's repository file [common.gitlab-ci.yml](https://framagit.org/aktivisda/aktivisda/-/blob/main/common.gitlab-ci.yml). Your `gitlab-ci.yml` automaticaly include this file.

These scripts require:

-   one _powerful_ gitlab runner (at least 4 vcpu and 4 Go RAM): you can install a gitlab runner on your own computer : pre-generation is currently done with Chrome ;
-   a FTP access to the files where you serve aktivisda. CI/CD variables:
    -   `FTP_PORT`
    -   `FPT_HOSTNAME`
    -   `FTP_USERNAME`
    -   `FTP_PASSWORD`

If you want you can redeploy your aktivisda instance after each update of aktivisda software. In order to do that, you have to ask the aktividda's team to add your instance in their `.gitlab-ci.ym`.
Please prepare:

-   the gitlab `PROJECT ID` of your repo
-   a _trigger token_: open the _Settings/CI-CD/Pipeline triggers_

## Server configuration

### Nginx

When someone asks for an url (ex. `/en/symbols/`), Nginx will serve the file `/en/symbols/index.html`. This file exist because we use pre-rendering but we do not pre-render all files, in particular the _symbols_ pages (`/en/symbols/<symbolid>`) are not pre-rendered. It means that without any particular configuration, Nginx will return a "Server Error".

You can fix it by appending `/index.html` to the `try_files` options.

```txt
location / {

    # Path to source
    alias /var/www/<your project>/www/;

    # Default indexes and catch-all
    index index.html index.php;
    try_files $uri $uri/ /index.html;

    # Prevent useless logs
    location = /favicon.ico {
        log_not_found off;
        access_log off;
    }
    location = /robots.txt {
        allow all;
        log_not_found off;
        access_log off;
    }

    # Deny access to hidden files and directories
    location ~ ^/(.+/|)\.(?!well-known\/) {
        deny all;
    }
}

```
