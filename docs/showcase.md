# Showcase − they use Aktivisda

> Do you want to host your own Aktivisda? Send an email to dev@aktivisda.earth and get dedicated support.

## Extinction Rebellion

Extinction Rebellion is a decentralised, international and politically non-partisan movement using non-violent direct action and civil disobedience to persuade governments to act justly on the Climate and Ecological Emergency.

They use Aktivisda all over the world from 2020. Their instances has more than 500 symbols (and many logos for all local groups).

Have a look to the instance: https://extinctionrebellion.aktivisda.earth

## AMAP

Have a look to the instance: https://amap.aktivisda.earth

## Alternatiba

Have a look to the instance: https://alternatiba.aktivisda.earth
