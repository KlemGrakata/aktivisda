import { useCredentialsStore } from '@/backtivisda/credentials';
import { Exception } from 'sass';

const localConfig = require('@/assets/local/localconfig.json');

const GITLAB_URL = process.env.VUE_APP_GITLAB_URL;
const GITLAB_REPO_URL = process.env.VUE_APP_GITLAB_REPO_URL;

const GITLAB_PROJECT_ID = process.env.VUE_APP_GITLAB_PROJECT_ID;

function gitlabHeaders() {
    const credentialsStore = useCredentialsStore();

    return {
        'PRIVATE-TOKEN': credentialsStore.gitlabToken,
        'Content-Type': 'application/json',
    };
}

const BRANCH = 'main';

export function gitlabRawUrl(filepath) {
    const url = `${GITLAB_URL}/${GITLAB_REPO_URL}/-/raw/${BRANCH}/${filepath}`;
    return url;
}

export function requestBlobFile(filepath) {
    const encodedFilepath = encodeURIComponent(filepath);
    const url = `${GITLAB_URL}/api/v4/projects/${GITLAB_PROJECT_ID}/repository/files/${encodedFilepath}/raw?ref=${BRANCH}`;

    return new Promise((resolve) => {
        fetch(url, { method: 'GET', headers: gitlabHeaders() })
            .then((response) => response.blob())
            .then((response) => resolve(response));
    });
}

export function requestFile(filepath) {
    const encodedFilepath = encodeURIComponent(filepath);
    const url = `${GITLAB_URL}/api/v4/projects/${GITLAB_PROJECT_ID}/repository/files/${encodedFilepath}/raw?ref=${BRANCH}`;

    return new Promise((resolve, reject) => {
        fetch(url, { method: 'GET', headers: gitlabHeaders() })
            .then((response) => {
                if (!response.ok) reject(new Exception(`Error status code ${response.status}`));
                return response.json();
            })
            .then((response) => resolve(response))
            .catch((error) => reject(error));
    });
}

// https://docs.gitlab.com/ee/api/commits.html

export function commit(actions, commit_message) {
    const body = {
        branch: BRANCH,
        commit_message: commit_message,
        author_email: localConfig.email,
        actions: actions,
    };

    const url = `${GITLAB_URL}/api/v4/projects/${GITLAB_PROJECT_ID}/repository/commits`;
    return new Promise((resolve, reject) => {
        fetch(url, { method: 'POST', headers: gitlabHeaders(), body: JSON.stringify(body) }).then((response) => {
            if (!response.ok) {
                reject();
                return;
            }
            resolve();
        });
    });
}
