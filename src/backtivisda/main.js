// Backtivisda

import Vue from 'vue';
import { createPinia, PiniaVuePlugin } from 'pinia';
import App from '@/backtivisda/App.vue';

import { i18n } from '@/plugins/i18n';
import '@/plugins/i18n-extended';
import '@/plugins/buefy';
import '@/plugins/localconfig';

import '@/assets/styles/main.scss';

import '@/plugins/vue-meta.js';

import router from '@/backtivisda/router';

import 'img-comparison-slider';

Vue.use(PiniaVuePlugin);
const pinia = createPinia();

// Tell Vue that the web component is present.
Vue.config.ignoredElements = [/img-comparison-slider/];

import LazyLoadDirective from '@/directives/LazyLoadDirective';

Vue.directive('lazyload', LazyLoadDirective);

Vue.config.productionTip = false;

Vue.prototype.$isBacktivisda = true;
Vue.prototype.$isAktivisda = false;

// App for Backvitisda
new Vue({
    pinia,
    router,
    i18n,
    render: (h) => h(App),
}).$mount('#app');
