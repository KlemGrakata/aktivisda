'use strict';

const localConfig = require('@/assets/local/localconfig.json');
import Vue from 'vue';

Vue.prototype.$localConfig = localConfig;
